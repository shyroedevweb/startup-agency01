import React from 'react';

import { Box, Container, Typography, Grid, Avatar, Card, CardHeader, CardContent, IconButton } from '@mui/material';


import Member1 from '../assets/team/member-1.png';
import Member2 from '../assets/team/member-2.png';
import Member3 from '../assets/team/member-3.png';
import Member4 from '../assets/team/member-4.png';


const TeamSection = () => {
    return (
        <Grid
        component="section" 
        sx={styles.section}        

        >
<Container>
            <Box>
            <Typography sx={{ textTransform: 'uppercase' }} textAlign="center" fontWeight="bold"
             
             color="primary.main" // working
             variant="body1" >
            Our Team
            </Typography>

            <Typography textAlign="center" fontWeight="500" color="text.primary" variant="h5" >
            The most qualified and talented individuals
            </Typography>
            </Box>     

            <Grid container 
             sx={styles.grid}
              >
                <Box sx={styles.card} >
                    <Avatar
                    src='/member-1.png'
                    alt="avatar1"
                    sx={styles.card.avatar}
                    />
                       
                    

                    <Typography fontWeight="500" color="text.primary" variant="h6" >
                    Saimon Harmer
            </Typography>

                    <Typography fontWeight="300"
             
            
             variant="body1" >
            CEO and Founder
            </Typography>

    


                </Box>

                {/* box 2 */}
                <Box sx={styles.card} >
                <Avatar
                    src="/member-2.png"
                    alt="avatar2"
                    sx={styles.card.avatar}
                    />
                       
                    

                    <Typography fontWeight="500" color="text.primary" variant="h6" >
                    Saimon Harmer
            </Typography>

                    <Typography fontWeight="300"
             
            
             variant="body1" >
            CEO and Founder
            </Typography>

    


                </Box>

                {/* box 3 */}
                <Box sx={styles.card} >
                <Avatar
                    src="/member-3.png"
                    alt="avatar3"
                    sx={styles.card.avatar}
                    />

                    <Typography fontWeight="500" color="text.primary" variant="h6" >
                    Saimon Harmer
            </Typography>

                    <Typography fontWeight="300"
             
            
             variant="body1" >
            CEO and Founder
            </Typography>

    


                </Box>

                {/* box 4 */}
                <Box sx={styles.card} >
                <Avatar
                    src="/member-4.png"
                    alt="avatar4"
                    sx={styles.card.avatar}
                    />

                    <Typography fontWeight="500" color="text.primary" variant="h6" >
                    Saimon Harmer
            </Typography>

                    <Typography fontWeight="300"
             
            
             variant="body1" >
            CEO and Founder
            </Typography>

    


                </Box>


            </Grid>

      

      
            
        </Container>
        </Grid>
    )
}

export default TeamSection

const styles = {
    section: {
        width: '100%',
        height: '100%',       
        // background: `url(${PatternBg})`,       
        paddingTop: '125px',
        paddingBottom: '100px',        
    },
    grid: {
        display: 'grid',
        gridTemplateColumns: 'repeat(4, 1fr)',
        marginTop: '40px',
        columnGap: '7%',
        rowGap: '5%',

    },
    paragraph: {
        fontSize: '20px',
        fontWeight: '400',      
        color: 'text.primary'
       
    },  
        card: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        rowGap: '5px',
        // border: '2px solid green',
        alignItems: 'flex-start',
        mt: '35px',
        avatar: {
            // fontSize: '25px',
            // borderRadius: '50%',
            // margin: '0px',
            // padding: '0px',
            width: '130px',
            height: '130px',
            border: '3px solid crimson',
            mb: '25px',
            // objectFit: 'cover',
            // objectPosition: 'center center'
            backgroundSize: 'cover',
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat',
            display: 'grid',

        }
    },
    
};
