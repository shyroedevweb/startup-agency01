import React from 'react';

import { Box, Container, Typography, Grid, Card, CardHeader, CardContent, IconButton } from '@mui/material';
import { PlayCircle } from '@mui/icons-material';
import Image from 'next/image';
import { StyledButtonPrimary } from './GlobalComponents';

import PatternBg from '../assets/patternBG.png';
import serviceShapeUrl from '../assets/shape-pattern1.png';
import iconSubscription from '../assets/feature/subscription.svg';
import iconPartnership from '../assets/feature/partnership.svg';

const ServiceSection = () => {
    return (
        <Grid
        component="section" 
        sx={styles.section}        

        >
<Container>
            <Box>
            <Typography sx={{ textTransform: 'uppercase', opacity: '0.7' }} textAlign="center" fontWeight="bold"
             
             color="white" // working
             variant="body1" >
            Core features
            </Typography>

            <Typography textAlign="center" fontWeight="500" color="white" variant="h5" >
            Smart Jackpots that you may love this anytime & anywhere
            </Typography>
            </Box>     

            <Grid container 
             sx={styles.grid}
              >
                <Box sx={styles.card} >
                    <Box sx={styles.card.numberBox} >
                        01
                    </Box>

                    <Typography fontWeight="500" color="white" variant="h6" >
                    Set disbursement Instructions
            </Typography>

                    <Typography fontWeight="300"
             
             color="white" // working
             variant="body1" >
            Get your blood tests delivered at home collect a sample from the your blood tests.
            </Typography>

    


                </Box>

                {/* box 2 */}
                <Box sx={styles.card} >
                    <Box sx={styles.card.numberBox} >
                        02
                    </Box>

                    <Typography fontWeight="500" color="white" variant="h6" >
                    Set disbursement Instructions
            </Typography>

                    <Typography fontWeight="300"
             
             color="white" // working
             variant="body1" >
            Get your blood tests delivered at home collect a sample from the your blood tests.
            </Typography>

    


                </Box>

                {/* box 3 */}
                <Box sx={styles.card} >
                    <Box sx={styles.card.numberBox} >
                        03
                    </Box>

                    <Typography fontWeight="500" color="white" variant="h6" >
                    Set disbursement Instructions
            </Typography>

                    <Typography fontWeight="300"
             
             color="white" // working
             variant="body1" >
            Get your blood tests delivered at home collect a sample from the your blood tests.
            </Typography>

    


                </Box>

                {/* box 4 */}
                <Box sx={styles.card} >
                    <Box sx={styles.card.numberBox} >
                        04
                    </Box>

                    <Typography fontWeight="500" color="white" variant="h6" >
                    Set disbursement Instructions
            </Typography>

                    <Typography fontWeight="300"
             
             color="white" // working
             variant="body1" >
            Get your blood tests delivered at home collect a sample from the your blood tests.
            </Typography>

    


                </Box>


            </Grid>

      

      
            
        </Container>
        </Grid>
    )
}

export default ServiceSection

const styles = {
    section: {
        width: '100%',
        height: '100%',
        backgroundColor: 'primary.main',
        // background: `url(${PatternBg})`,       
        paddingTop: '125px',
        paddingBottom: '100px',        
    },
    grid: {
        display: 'grid',
        gridTemplateColumns: 'repeat(4, 1fr)',
        marginTop: '40px',
        columnGap: '7%',
        rowGap: '5%',

    },
    paragraph: {
        fontSize: '20px',
        fontWeight: '400',      
        color: 'text.primary'
       
    },  
        card: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        rowGap: '20px',
        // border: '2px solid green',
        alignItems: 'flex-start',
        mt: '35px',
        numberBox: {
            // fontSize: '25px',
            width: '70px',
            height: '70px',
            borderRadius: '40%',
            fontSize: '24px',
            fontWeight: 'bold',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
            color: '#234582'

        }
    },
    
};
