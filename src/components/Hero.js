import React from 'react';
import { Box, Container, Typography } from '@mui/material';
import Image from 'next/image';

import heroImgSrc from '../assets/banner-thumb.png';

import { StyledButtonPrimary } from './GlobalComponents';


const Hero = () => {
    return (
        <Container
            maxWidth="sm"
            sx={ styles.container }
            // sx={{
            //     display: 'flex',
            //     flexDirection: 'column',
            //     justifyContent: 'flex-start',
            //     alignItems: 'center',
            //     rowGap: '25px',
                
            // }}
        >
            <Typography
            textAlign="center"
            fontWeight="bold"
            variant="h2" >
            Top Quality Digital Products To Explore
            </Typography>
            <Typography
            // sx={ styles.paragraph } // working
            sx={ styles.paragraph, styles.paragraphDestaque } // working
            // sx={ styles.paragraph, { color: 'blue' } }  // working         
      
            variant="body1" >
            Get your blood tests delivered at let home collect sample from the victory of the managements that supplies best design system guidelines ever.
            </Typography>
            <StyledButtonPrimary variant="contained" >
                Explore
            </StyledButtonPrimary>

            <Image src={heroImgSrc} alt="hero-image" />
            
        </Container>
    )
}

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        rowGap: '25px',
    },
    paragraph: {
        fontSize: '20px',
        fontWeight: '400',
        textAlign: 'center',
        //color: 'green'
        // lineHeight: '1.9',
    },
    paragraphDestaque: {
        // color: 'orangered'
        color: 'secondary.main'
    }
}

export default Hero;
