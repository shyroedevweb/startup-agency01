import React from 'react';

import { Box, Container, Typography, Grid, Card, CardHeader, CardContent, IconButton } from '@mui/material';
import { PlayCircle } from '@mui/icons-material';
import Image from 'next/image';
import { StyledButtonPrimary } from './GlobalComponents';

import coreImageUrl from '../assets/core-feature.png';
import serviceShapeUrl from '../assets/shape-pattern1.png';
import iconSubscription from '../assets/feature/subscription.svg';
import iconPartnership from '../assets/feature/partnership.svg';

const ServiceSection = () => {
    return (
        <Container
        sx={styles.section}        

        >
      

        <Container sx={styles.containerContent} >
        <Typography sx={{ textTransform: 'uppercase' }}  fontWeight="bold"
             //color="#EA3a60"
             //color="text.destaque"   // working
             color="primary.main" // working
             variant="body1" >
            Core features
            </Typography>
            <Typography fontWeight="500" variant="h3" >
            Smart Jackpots that you may love this anytime & anywhere
            </Typography>

            <Typography sx={styles.paragraph} variant="body1" >
            Get your tests delivered at let home collect sample from the victory of the managements that supplies best design system guidelines ever.
            </Typography>

            <StyledButtonPrimary sx={{ mt: '20px' }} variant="contained" >
                Get Started
                </StyledButtonPrimary>
         
        </Container>

        <Box sx={styles.imageContainer} >
            <Image sx={{ width: '100%', height: '100%' }} src={coreImageUrl} alt="image-service" objectFit="cover" />
            
            <Box sx={styles.shape} >
                <Image src={serviceShapeUrl} alt="shape-image"  objectFit="contain" />
            </Box>
        </Box>
            
        </Container>
    )
}

export default ServiceSection

const styles = {
    section: {
        width: '100%',
        height: '100%',       
        display: 'flex',
        paddingLeft: '0px',
        paddingBottom: '100px'
    },
    paragraph: {
        fontSize: '20px',
        fontWeight: '400',      
        color: 'text.primary'
       
    },
    imageContainer: {
        // width: '50%',
        // paddingRight: '50px',
        position: 'relative',
    },
    iconBtn: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%,-50%)',
        // width: '50px',
        // height: '50px',
        fontSize: '50px',
        zIndex: 20,
    },
    containerContent: {        
        width: '50%',       
       
        paddingTop: '80px',
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        rowGap: '25px',
        alignItems: 'flex-start'
    },
    card: {
        display: 'flex',
        columnGap: '20px',
        // border: '2px solid green',
        alignItems: 'flex-start',
        mt: '35px',
        icon: {
            // fontSize: '25px',
            width: '160px',
            height: '160px',
        }
    },
    shape: {
        position: 'absolute',
        bottom: '-20%',
        right: '-20%',
        width: '350px',
        height: '350px',
        transform: 'rotate(240deg)',
       // transform: 'translate(-50%,-50%)',
        zIndex: -1,       

    }
};
