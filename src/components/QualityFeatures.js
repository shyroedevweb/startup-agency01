import React from "react";

import {
  Box,
  Container,
  Typography,
  Grid,
  Card,
  Item,
  Paper,
  CardHeader,
  CardContent,
  IconButton,
} from "@mui/material";
import { PlayCircle } from "@mui/icons-material";
import Image from "next/image";

import iconSubscription from '../assets/feature/partnership.svg';
import iconPartnership from '../assets/feature/subscription.svg';
import iconPerformance from '../assets/feature/performance.svg';
import iconSupport from '../assets/feature/support.svg';

const QualityFeatures = () => {
  return (
    <Container sx={styles.section} >
      <Box>
        <Typography
          textAlign="center"
          sx={{ textTransform: "uppercase" }}
          fontWeight="bold"
          //color="#EA3a60"
          //color="text.destaque"   // working
          color="primary.main" // working
          variant="body1"
        >
          Quality Features
        </Typography>
        <Typography textAlign="center" fontWeight="bold" variant="h3">
          Amazing useful features
        </Typography>
      </Box>

        <Grid
        container
        mt="80px"
        rowSpacing={8}
        columnSpacing={8}
        
        >
            <Grid container item lg={6} >
                {/* <Item> */}
            <Paper sx={styles.card} >
                <Box sx={styles.card.icon} >
                <Image                 
                    src={iconPerformance} alt="icon-subscription" />
                </Box>
                <Box>
                <Typography sx={{ mb: '15px' }}  fontWeight="bold"
             
             color="text.primary" // working
             variant="h6" >
            Fast Performance
            </Typography>
            <Typography variant="body1" >
            Let’s just get this out of the way - there will always be a kit version of Edu flow. Paid subscriptions allow us to continue helping learners around the world.
            </Typography>
                </Box>
            </Paper>
            {/* </Item> */}

            </Grid>

            {/* Grid 2 */}

            <Grid item lg={6} >
                {/* <Item> */}
            <Paper sx={styles.card} >
                <Box sx={styles.card.icon} >
                <Image                 
                    src={iconSubscription} alt="icon-subscription" />
                </Box>
                <Box>
                <Typography sx={{ mb: '15px' }}  fontWeight="bold"
             
             color="text.primary" // working
             variant="h6" >
            Pro Subscription
            </Typography>
            <Typography variant="body1" >
            Let’s just get this out of the way - there will always be a kit version of Edu flow. Paid subscriptions allow us to continue helping learners around the world.
            </Typography>
                </Box>
            </Paper>
            {/* </Item> */}

            </Grid>

            {/* grid 3 */}

            <Grid item lg={6} >
            <Card sx={styles.card} >
                <Box sx={styles.card.icon} >
                <Image                 
                    src={iconPartnership} alt="icon-subscription" />
                </Box>
                <Box>
                <Typography sx={{ mb: '15px' }}  fontWeight="bold"
             
             color="text.primary" // working
             variant="h6" >
                 Partnership deal
            </Typography>
            <Typography variant="body1" >
            Let’s just get this out of the way - there will always be a kit version of Edu flow. Paid subscriptions allow us to continue helping learners around the world.
            </Typography>
                </Box>
            </Card>

            </Grid>
    {/* grid 4 */}

    <Grid item lg={6} >
            <Card sx={styles.card} >
                <Box sx={styles.card.icon} >
                <Image                 
                    src={iconSupport} alt="icon-subscription" />
                </Box>
                <Box>
                <Typography sx={{ mb: '15px' }}  fontWeight="bold"
             
             color="text.primary" // working
             variant="h6" >
            Customer Support
            </Typography>
            <Typography variant="body1" >
            We believe it’s important for everyone to have access to software – especially when it comes to digital learning be navigated by keyboard and screen readers.
            </Typography>
                </Box>
            </Card>

            </Grid>



        </Grid>

    </Container>
  );
};

export default QualityFeatures;


const styles = {
    section: {
        width: '100%',
        height: '100%',               
        padding: '100px 0px',
    },   
    card: {
        display: 'flex',
        columnGap: '20px',
        // border: '2px solid green',
        alignItems: 'flex-start',
        mt: '35px',
        height: 'auto',
        padding: '0px',
        icon: {
            // fontSize: '25px',
            width: '300px',
            height: '300px',
        }
    },
   
};
