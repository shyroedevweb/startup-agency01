import React from "react";

import { Navigation, Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from "swiper/react";
import {
  Box,
  Container,
  Typography,
  Grid,
  Rating,
  Avatar,
  Card,
  CardHeader,
  CardContent,
  IconButton,
} from "@mui/material";
import { ArrowCircleLeft, ArrowCircleRight } from '@mui/icons-material';
import Image from "next/image";

// Import Swiper styles
import "swiper/css";
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';




import Avatar1 from "../assets/testimonial/avatar1.png";
import Avatar2 from "../assets/testimonial/avatar2.png";
import Avatar3 from "../assets/testimonial/avatar3.png";
import Avatar4 from "../assets/testimonial/avatar4.png";

const TeamSection = () => {
    const navigationPrevRef = React.useRef(null);
    const navigationNextRef = React.useRef(null);
  return (
    <Grid component="section" sx={styles.section}>
      <Container>
        <Box sx={styles.header} >
          <Typography
            sx={{ textTransform: "uppercase" }}
            textAlign="center"
            fontWeight="bold"
            color="primary.main" // working
            variant="body1"
          >
            Testimonial
          </Typography>

          <Typography
            textAlign="center"
            fontWeight="500"
            color="text.primary"
            variant="h5"
          >
            Meet Client Satisfaction
          </Typography>
        </Box>

        {/* slider */}

        <Box>
          <Swiper
            modules={[Navigation, Scrollbar]}
            //navigation={ {prevEl: ".swiper-navigation-prev", nextEl: ".swiper-navigation-next" }} // not working
            navigation={ {prevEl: navigationPrevRef.current, nextEl: navigationNextRef.current }}
            onBeforeInit={(swiper) => {
                swiper.params.navigation.prevEl = navigationPrevRef.current;
                swiper.params.navigation.nextEl = navigationNextRef.current;
           }}
            
            //scrollbar={{ draggable: true }}
            spaceBetween={50}
            slidesPerView={3}
            onSlideChange={() => console.log("slide change")}
            onSwiper={(swiper) => console.log(swiper)}
          >
            <SwiperSlide>
              <Rating name="simple-controlled" readOnly value="4" />
              <Typography fontWeight="500" color="text.primary" variant="h6">
                Modern look & trending design
              </Typography>

              <Typography fontWeight="300" variant="body1">
                Get working experience to work with this amazing team & in
                future want to work together for bright future projects and also
                make deposit to freelancer.
              </Typography>

              <Box sx={styles.avatarContainer}>
                <Box>
                  <Image src={Avatar1} altImage="photo1" />
                </Box>
                <Box>
                  <Typography
                    fontWeight="500"
                    color="text.primary"
                    variant="h6"
                  >
                    Denny Hilguston
                  </Typography>

                  <Typography
                    fontWeight="500"
                    color="primary.main"
                    variant="body1"
                  >
                    @denny.hil
                  </Typography>
                </Box>
              </Box>
            </SwiperSlide>

            {/* slide 2 */}
            <SwiperSlide>
              <Rating name="simple-controlled" readOnly value="4" />
              <Typography fontWeight="500" color="text.primary" variant="h6">
                Modern look & trending design
              </Typography>

              <Typography fontWeight="300" variant="body1">
                Get working experience to work with this amazing team & in
                future want to work together for bright future projects and also
                make deposit to freelancer.
              </Typography>

              <Box sx={styles.avatarContainer}>
                <Box>
                  <Image src={Avatar2} altImage="photo2" />
                </Box>
                <Box>
                  <Typography
                    fontWeight="500"
                    color="text.primary"
                    variant="h6"
                  >
                    Denny Hilguston
                  </Typography>

                  <Typography
                    fontWeight="500"
                    color="primary.main"
                    variant="body1"
                  >
                    @denny.hil
                  </Typography>
                </Box>
              </Box>
            </SwiperSlide>
            {/* slide 3 */}
            <SwiperSlide>
              <Rating name="simple-controlled" readOnly value="4" />
              <Typography fontWeight="500" color="text.primary" variant="h6">
                Modern look & trending design
              </Typography>

              <Typography fontWeight="300" variant="body1">
                Get working experience to work with this amazing team & in
                future want to work together for bright future projects and also
                make deposit to freelancer.
              </Typography>

              <Box sx={styles.avatarContainer}>
                <Box>
                  <Image src={Avatar3} altImage="photo3" />
                </Box>
                <Box>
                  <Typography
                    fontWeight="500"
                    color="text.primary"
                    variant="h6"
                  >
                    Denny Hilguston
                  </Typography>

                  <Typography
                    fontWeight="500"
                    color="primary.main"
                    variant="body1"
                  >
                    @denny.hil
                  </Typography>
                </Box>
              </Box>
            </SwiperSlide>
            {/* slide 4 */}
            <SwiperSlide>
              <Rating name="simple-controlled" readOnly value="4" />
              <Typography fontWeight="500" color="text.primary" variant="h6">
                Modern look & trending design
              </Typography>

              <Typography fontWeight="300" variant="body1">
                Get working experience to work with this amazing team & in
                future want to work together for bright future projects and also
                make deposit to freelancer.
              </Typography>

              <Box sx={styles.avatarContainer}>
                <Box>
                  <Image src={Avatar4} altImage="photo4" />
                </Box>
                <Box>
                  <Typography
                    fontWeight="500"
                    color="text.primary"
                    variant="h6"
                  >
                    Denny Hilguston
                  </Typography>

                  <Typography
                    fontWeight="500"
                    color="primary.main"
                    variant="body1"e
                  >
                    @denny.hil
                  </Typography>
                </Box>
              </Box>
            </SwiperSlide>
            {/* slide 5 */}
            <SwiperSlide>
              <Rating name="simple-controlled" readOnly value="4" />
              <Typography fontWeight="500" color="text.primary" variant="h6">
                Modern look & trending design5
              </Typography>

              <Typography fontWeight="300" variant="body1">
                Get working experience to work with this amazing team & in
                future want to work together for bright future projects and also
                make deposit to freelancer.
              </Typography>

              <Box sx={styles.avatarContainer}>
                <Box>
                  <Image src={Avatar1} altImage="photo1" />
                </Box>
                <Box>
                  <Typography
                    fontWeight="500"
                    color="text.primary"
                    variant="h6"
                  >
                    Denny Hilguston
                  </Typography>

                  <Typography
                    fontWeight="500"
                    color="primary.main"
                    variant="body1"
                  >
                    @denny.hil
                  </Typography>
                </Box>
              </Box>
            </SwiperSlide>
            {/* slide 6 */}
            <SwiperSlide>
              <Rating name="simple-controlled" readOnly value="4" />
              <Typography fontWeight="500" color="text.primary" variant="h6">
                Modern look & trending design  - 6
              </Typography>

              <Typography fontWeight="300" variant="body1">
                Get working experience to work with this amazing team & in
                future want to work together for bright future projects and also
                make deposit to freelancer.
              </Typography>

              <Box sx={styles.avatarContainer}>
                <Box>
                  <Image src={Avatar2} alt="pho2" />
                </Box>
                <Box>
                  <Typography
                    fontWeight="500"
                    color="text.primary"
                    variant="h6"
                  >
                    Denny Hilguston
                  </Typography>

                  <Typography
                    fontWeight="500"
                    color="primary.main"
                    variant="body1"
                  >
                    @denny.hil
                  </Typography>
                </Box>
              </Box>
            </SwiperSlide>            

            {/* navigation */}
            <Box sx={styles.swiperNavigation} >
                <IconButton 
                 //className="swiper-button-prev swiper-navigation-prev" 
                 ref={navigationPrevRef}
                  size="medium" >
                    <ArrowCircleLeft
                     //className="swiper-navigation-prev"
                       sx={styles.swiperNavigation.icon} />
                </IconButton>
                <IconButton
                 //className="swiper-button-next swiper-navigation-next"
                  ref={navigationNextRef}
                  color="success" size="medium"   >
                    <ArrowCircleRight
                     //className="swiper-navigation-next"
                      sx={styles.swiperNavigation.icon} />
                </IconButton>
            </Box>
          </Swiper>
        </Box>
      </Container>
    </Grid>
  );
};

export default TeamSection;

const styles = {
  section: {
    width: "100%",
    height: "100%",
    // background: `url(${PatternBg})`,
    paddingTop: "125px",
    paddingBottom: "100px",
  },
  header: {
    mb: '100px',
  }, 
  paragraph: {
    fontSize: "20px",
    fontWeight: "400",
    color: "text.primary",
  },
  avatarContainer: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    columnGap: "20px",
    mt: "25px",
  },
  swiperNavigation: {
    //border: '2px solid orangered',
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    columnGap: '15px',
    alignItems: 'center',
    pt: '30px',
    icon: {
        fontSize: '40px',
        color: 'primary.main',
    },
  }, 
};
